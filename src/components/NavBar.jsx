import React from "react";

export default function NavBar() {
  return (
    <div>
      <nav className="navbar navbar-light bg-primary mb-5">
        <div className="container">
          <a href="#" className="navbar-brand text-light">KSHRD Student</a>
          <form className="d-flex">
            <input
              className="form-control me-2"
              type="search"
              placeholder="Search"
              aria-label="Search"
            />
            <button className="btn btn-outline-light" type="submit">
              Search
            </button>
          </form>
        </div>
      </nav>
    </div>
  );
}
