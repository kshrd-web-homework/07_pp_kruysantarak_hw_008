import React from "react";
import "bootstrap/dist/css/bootstrap.css";
import { AiOutlineUser } from "react-icons/ai";

export default function Profile(props) {
  let save = () => {
    if (emptyFieldValidation() || emailValidation() || passwordValidation()) {
      return;
    }
    let username = document.querySelector("#usernameInput");
    let gender_value;
    if (document.querySelector("#rd-male").checked) {
      gender_value = document.querySelector("#rd-male").value;
    } else {
      gender_value = document.querySelector("#rd-female").value;
    }
    let email = document.querySelector("#emailInput");
    let password = document.querySelector("#passwordInput");
    let obj = {
      username: username.value.trim(),
      gender: gender_value,
      email: email.value.trim(),
      password: password.value,
      selected: false,
    };
    props.onAdd(obj);

    clearInput(username, email, password);
  };

  // empty field validation
  let emptyFieldValidation = () => {
    let email = document.getElementById("emailInput");
    let username = document.getElementById("usernameInput");
    let password = document.getElementById("passwordInput");
    let userHelp = document.getElementById("userHelp");
    let emailHelp = document.getElementById("emailHelp");
    let pwdHelp = document.getElementById("pwdHelp");
    let isEmpty = true;

    if (username.value.trim() === "") {
      username.classList.add("is-invalid");
      userHelp.innerText = "Username can not be empty.";
    } else {
      username.classList.remove("is-invalid");
      userHelp.innerText = "";
    }
    if (email.value.trim() === "") {
      email.classList.add("is-invalid");
      emailHelp.innerText = "Email can not be empty.";
    } else if (!regex.test(email.value)) {
      email.classList.add("is-invalid");
      emailHelp.innerText = "Invalid email address.";
    } else {
      email.classList.remove("is-invalid");
      emailHelp.innerText = "";
    }
    if (password.value === "") {
      password.classList.add("is-invalid");
      pwdHelp.innerText = "Password can not be empty.";
    } else {
      password.classList.remove("is-invalid");
      pwdHelp.innerText = "";
    }
    if (
      username.value.trim() === "" ||
      email.value.trim() === "" ||
      password.value === ""
    ) {
      isEmpty = true;
    } else {
      isEmpty = false;
    }
    return isEmpty;
  };

  // clear input
  let clearInput = (username, email, password) => {
    username.value = "";
    email.value = "";
    password.value = "";
  };

  // email invalid
  let regex = /^[^\s@]+@([^\s@.,]+\.)+[^\s@.,]{2,}$/;
  let emailValidation = () => {
    let isValid = false;
    let email = document.querySelector("#emailInput");
    let emailHelp = document.querySelector("#emailHelp");
    if (!regex.test(email.value)) {
      email.classList.add("is-invalid");
      emailHelp.innerText = "Invalid email address.";
      isValid = true;
    } else {
      email.classList.remove("is-invalid");
      emailHelp.innerText = "";
      isValid = false;
    }
    if (email.value === "") {
      email.classList.remove("is-invalid");
      emailHelp.innerText = "";
    }
    return isValid;
  };

  // username invalid
  let usernameValidation = () => {
    let username = document.querySelector("#usernameInput");
    let userHelp = document.querySelector("#userHelp");
    if (username.value.trim() === "" || username.value.trim() !== "") {
      username.classList.remove("is-invalid");
      userHelp.innerText = "";
    }
  };

  // password invalid
  let passwordValidation = () => {
    let password = document.querySelector("#passwordInput");
    let pwdHelp = document.querySelector("#pwdHelp");
    let isCorrect = false;
    if(password.value.length <= 8){
      password.classList.add("is-invalid");
      pwdHelp.innerText = "Password must contain at least 8 characters.";
      isCorrect = true;
    }else if (password.value === "" || password.value !== "") {
      password.classList.remove("is-invalid");
      pwdHelp.innerText = "";
      isCorrect = false;
    }
    return isCorrect;
  };

  return (
    <div>
      <form>
        {/* icon */}
        <div className="text-center mb-3">
          <AiOutlineUser fontSize="100px" className="mb-2" />
          <h5>Create Account</h5>
        </div>
        {/* username */}
        <div className="mb-3">
          <label htmlFor="username" className="form-label">
            Username
          </label>
          <input
            type="text"
            className="form-control"
            id="usernameInput"
            aria-describedby="userlHelp"
            placeholder="Username"
            onChange={usernameValidation}
          />
          <p id="userHelp" className="form-text invalid-feedback"></p>
        </div>
        {/* Gender */}
        <label className="form-label">Gender</label>
        <div className="mb-3" id="gender">
          <div className="form-check form-check-inline">
            <input
              className="form-check-input checked"
              type="radio"
              id="rd-male"
              value="male"
              name="rd-gender"
              onChange={(e) => {}}
              checked
            />
            <label className="form-check-label" htmlFor="rd-male">
              male
            </label>
          </div>
          <div className="form-check form-check-inline">
            <input
              className="form-check-input"
              type="radio"
              id="rd-female"
              value="female"
              name="rd-gender"
            />
            <label className="form-check-label" htmlFor="rd-female">
              female
            </label>
          </div>
        </div>
        {/* email */}
        <div className="mb-3">
          <label htmlFor="emailInput" className="form-label">
            Email
          </label>
          <input
            type="email"
            className="form-control"
            id="emailInput"
            aria-describedby="emailHelp"
            placeholder="Example@gmail.com"
            onChange={emailValidation}
          />
          <p id="emailHelp" className="form-text invalid-feedback"></p>
        </div>
        {/* password */}
        <div className="mb-3">
          <label htmlFor="passwordInput" className="form-label">
            Password
          </label>
          <input
            type="password"
            className="form-control"
            id="passwordInput"
            placeholder="Password"
            onChange={passwordValidation}
          />
          <p id="pwdHelp" className="form-text invalid-feedback"></p>
        </div>
        <button type="button" className="btn btn-primary mb-3" onClick={save}>
          Save
        </button>
      </form>
    </div>
  );
}
