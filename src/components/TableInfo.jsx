import React from "react";

export default function TableInfo(props) {
  let data = props.data;
  let selectRow = (index) => {
    let data = props.data;
    let firstInput = document.getElementById(`${index}`);
    firstInput.checked = !firstInput.checked;
    if (firstInput.checked) {
      firstInput.parentElement.parentElement.classList.add("selectedRow");
      data[index].selected = true;
    } else {
      firstInput.parentElement.parentElement.classList.remove("selectedRow");
      data[index].selected = false;
    }
    props.onSelected(data);
  };

  let deleteRow = () => {
    props.onDelete();
    let row = document.querySelector(".selectedRow");
    row.classList.remove("selectedRow");
    row.firstElementChild.checked = false;
  };

  return (
    <div>
      <h3 className="mt-5 mb-3">Table Account</h3>
      <table className="table" id="table-data">
        <thead>
          <tr className="bg-success text-light">
            <th scope="col">#</th>
            <th scope="col">Username</th>
            <th scope="col">Email</th>
            <th scope="col">Gender</th>
          </tr>
        </thead>
        <tbody>
          {data.map((item, index) => (
              <tr
                key={index}
                onClick={selectRow.bind(this, index)}
                className="table-row"
              >
                <td style={{ display: "none" }}>
                  <input type="checkbox" id={index} />
                </td>
                <th scope="row">{index + 1}</th>
                <td>{item.username}</td>
                <td>{item.email}</td>
                <td>{item.gender}</td>
              </tr>
          ))}
        </tbody>
      </table>
      <button type="button" className="btn btn-danger mb-3" onClick={deleteRow}>
        delete
      </button>
    </div>
  );
}
