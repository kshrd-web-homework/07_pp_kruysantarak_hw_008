import React from "react";
import Profile from "./Profile";
import TableInfo from "./TableInfo";

export default function ContentPage(props) {
  return (
    <div>
      <div className="row">
        <div className="col-lg-4 col-sm-12">
          <Profile onAdd={props.onAdd}/>
        </div>
        <div className="col-lg-8 col-sm-12">
          <TableInfo 
            onDelete={props.onDelete} 
            onSelected={props.onSelected}
            data={props.data}/>
        </div>
      </div>
    </div>
  );
}
