import "bootstrap/dist/css/bootstrap.css";
import React, { Component } from "react";
import { Container } from "react-bootstrap";
import ContentPage from "./components/ContentPage";
import NavBar from './components/NavBar'
import './app.css'

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      data: [
        {
          username : 'Santarak',
          gender : 'male', 
          email : 'k.santarak@gmail.com',
          password : 'incorrect', 
          selected : false
        },
        {
          username : 'Sothearoth',
          gender : 'male', 
          email : 'sothearoth@gmail.com',
          password : '123qpoi', 
          selected : false
        },
        {
          username : 'Maphineth',
          gender : 'male', 
          email : 'maphineth@gmail.com',
          password : 'qwertyu', 
          selected : false
        }
      ],
    };
  }
  onAdd = (props) => {
    let data = this.state.data;
    data.push(props);
    this.setState({
      data: data
    });
  };

  onSelected = (props) => {
    this.setState({
      data : props
    })
  };

  onDelete = () => {
    let data = this.state.data;
    data= data.filter(item => {
      return item.selected === false;
    });
    this.setState({
      data : data
    })
  };

  render() {
    return (
      <div>
        <NavBar/>
        <Container>
          <ContentPage 
            onAdd={this.onAdd} 
            onDelete={this.onDelete} 
            onSelected={this.onSelected}
            data={this.state.data}/>
        </Container>
      </div>
    );
  }
}
